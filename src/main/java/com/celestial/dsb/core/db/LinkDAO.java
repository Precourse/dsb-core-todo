package com.celestial.dsb.core.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import domain.Bookmark;
import java.sql.CallableStatement;

public class LinkDAO
{

    Connection con = null;

    private static String linkTableName = "link";
    private static String joiningTable = "linkToMetatag";
    private static String tagTableName = "metatag";

    public void saveLink(String name, String description, ArrayList<String> tags) throws SQLException
    {
        PreparedStatement saveStmt = null;
        // SQL statement to insert into the link table
            String linkSql = "insert into " + linkTableName + " (link_name, link_description) values( ?, ?)";

        //++ insert code here
        try
        {
            // Get the DB connection, prepare the linkSql statement, assign values to
            // each ? parameter -
            // name to param 1
            // description to param 2
            //++[5] insert code here
        	con = DBConnector.getConnector().getConnection();
            saveStmt = con.prepareStatement(linkSql);
            saveStmt.setString(1, name);
            saveStmt.setString(2, description);
            
            // Setting AutoCommit to false means we can execute multiple SQL
            // statememts in our code in a single executeUpdate() call
            con.setAutoCommit(false);
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            
            // This next line creates a call to a stored procedure
            CallableStatement cStmt = con.prepareCall("{call saveTag(?)}");
            int linkId = getCurrentIDNum() + 1;
            
            // Iterate over all the tags
            for (String tag : tags)
            {
                // SQL statement to insert into the linkToMetatag table
                String query = "insert into linkToMetatag (tag_name, link_id) values ('" + tag + "', " + linkId + " )";//++ insert code here
                
                // This call adds the call to the stored procedure to a batch
                // of commands
                stmt.addBatch(query);
                
                // This next call sets the tag value to param 1 of the call
                // to the stored proceure
                cStmt.setString(1, tag);
                
                // This call adds the call to the stored procedure to a batch
                // of commands
                cStmt.addBatch();   
            }
            // Execute the insert statement to the link table
            saveStmt.executeUpdate();   //++[6] insert code here
            
            // Execute all the batched insert statements to the linkToMetatag
            // and call to stored procedure
            stmt.executeBatch();    //++[7] insert code here
            cStmt.executeBatch();  //++[7] insert code here

            // commit changes to the DB
            con.commit();
            stmt.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private int getCurrentIDNum()
    {
        int result = 0;
        try
        {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("select link_id from " + linkTableName);
            while (rs.next())
            {
                int idNum = rs.getInt("link_id");
                if (idNum > result)
                {
                    result = idNum;
                }
            }
        } catch (SQLException se)
        {
            System.out.println(se);
        }
        return result;
    }

    public ArrayList<Bookmark> getAllLinks()
    {
        PreparedStatement selectStmt = null;
        // The sql statement to get all links from the link table
        String sql =  "select * from " + linkTableName; //++[1] insert code here;
        ResultSet result;
        
        // Declare a collection of Bookmark that will hold the data from the result set
        //++[2] insert code here
        ArrayList<Bookmark> bookmarks = new ArrayList<>(); 
        try
        {
            Bookmark bookmark;
            // Get the DB connection, prepare rhe SQL statement, then execute the query
            //++[3] insert code here
            con = DBConnector.getConnector().getConnection(); 
            selectStmt = con.prepareStatement(sql); 
            result = selectStmt.executeQuery(); 
            while (result.next())
            {
                //++[4] insert code here
            	bookmark = new Bookmark( result.getInt(1), result.getString(2), result.getString(3)); 
            	bookmarks.add(bookmark); 
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            bookmarks = null;
        } 
        //System.out.println(Arrays.toString(bookmarks.toArray()));
        return bookmarks;
    }

    public ArrayList<Bookmark> getLinksForTag(String metatag)
    {
    	Bookmark bookmark;
        String sql = "SELECT * FROM " + linkTableName + " INNER JOIN " + joiningTable + " ON " + joiningTable + ".link_id = " + linkTableName
                + ".link_id where " + joiningTable + ".tag_name = ?";
        ResultSet result;
        PreparedStatement selectStmt = null;
        ArrayList<Bookmark> bookmarks = new ArrayList<>();
        try
        {
            con = DBConnector.getConnector().getConnection();
            selectStmt = con.prepareStatement(sql);
            selectStmt.setString(1, metatag);
            result = selectStmt.executeQuery();
            while (result.next())
            {
            	bookmark = new Bookmark(result.getInt(1), result.getString(2), result.getString(3));
                bookmarks.add(bookmark);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            bookmarks = null;
        } 
        return bookmarks;
    }
}
