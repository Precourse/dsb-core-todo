package com.celestial.dsb.core;
import java.util.regex.*;

public class TagManipulator {

	public TagManipulator(){
		
	}
	
	public String[] parseString(String tags, String regex){
		 String result[]; 
		 
	     Pattern pp = Pattern.compile(regex);         
	     result = pp.split(tags.trim()); 
	 
	     return result; 
	}
}
